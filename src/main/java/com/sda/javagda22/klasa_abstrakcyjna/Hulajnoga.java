package com.sda.javagda22.klasa_abstrakcyjna;

public class Hulajnoga extends Pojazd{
    public Hulajnoga(int iloscKol) {
        super(iloscKol);
    }

    // klasa bez metody abstrakcyjnej nie jest kompletna!
    public void zmienBiegNaWyzszy() {
    }
}
