package com.sda.javagda22.kolekcje;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class MainZadanie {
    public static void main(String[] args) {
        List<Integer> integers = new ArrayList<>();

        Scanner scanner = new Scanner(System.in);
        for (int i = 0; i < 5; i++) {
            integers.add(scanner.nextInt());
        }

        System.out.println(integers);

        Random generator= new Random();
        for (int i = 0; i < 5; i++) {
            integers.add(generator.nextInt());
        }

        System.out.println(integers);
    }
}
