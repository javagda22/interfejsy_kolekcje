package com.sda.javagda22.lombok_przyklad;

import lombok.Data;

@Data
public class ListaUwag {
    private String[] uwagi;
}
