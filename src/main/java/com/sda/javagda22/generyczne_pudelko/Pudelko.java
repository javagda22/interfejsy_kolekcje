package com.sda.javagda22.generyczne_pudelko;

public class Pudelko<T> {
    private T zawartość;

    public Pudelko() {
    }

    public Pudelko(T zawartość) {
        this.zawartość = zawartość;
    }

    public T getZawartość() {
        return zawartość;
    }

    public void setZawartość(T zawartość) {
        this.zawartość = zawartość;
    }

    public boolean czyPudelkoJestPuste() {
        return this.zawartość == null;
    }
}
