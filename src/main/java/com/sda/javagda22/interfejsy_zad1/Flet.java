package com.sda.javagda22.interfejsy_zad1;

public class Flet implements Instrumentalny, Gracz {
    @Override
    public void graj() {
        Instrumentalny.super.graj();
    }
}
