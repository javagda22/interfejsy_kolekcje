package com.sda.javagda22.interfejsy_zad1;

public class Main {
    public static void main(String[] args) {
        Gitara gitara = new Gitara();
//        gitara.graj();
        Pianino pianino = new Pianino();
//        pianino.graj();
        Bęben bęben = new Bęben();
//        bęben.graj();

        Instrumentalny[] instrumenty = new Instrumentalny[3];
        instrumenty[0] = gitara;
        instrumenty[1] = pianino;
        instrumenty[2] = bęben;

        // wypisać pętlą foreach
        for (Instrumentalny inst : instrumenty) {
            inst.graj();
        }

        // 1.
        // klasa może dziedziczyć po jednej innej klasie (niekoniecznie abstrakcyjnej)
        // klasa może implementować nieskończenie wiele interfejsów

        // 2.
        // interfejs nie ma pól (nie ma zmiennych klasy/interfejsu)
        // wszystkie pola interfejsu są publiczne statyczne

        // klasa abstrakcyjna może posiadać dowolne pola z różnymi modyfikatorami dostępu

        // 3.
        // interfejs ma metody które są ZAWSZE publiczne i abstrakcyjne
        // (nie posiadają implementacji)

        // klasa abstrakcyjna może mieć metody abstrakcyjne ale również może mieć
        // metody nie abstrakcyjne - takie które posiadają pewną implementacje

        // 4. (podobieństwo)
        // w obu przypadkach - nie może istnieć ani żaden obiekt interfejsu
        // (nie ma konstruktora i nie można go stworzyć) ani nie może istnieć obiekt
        // klasy abstrakcyjnej
        //
        // -- > (odstępstwem są klasy anonimowe o których będzie później)

        // -- > (w interfejsach od javy 8 można impementować metody domyślne)
        // -- > (czyli posiadające implementację domyślną)

        // ICloseable
    }
}
