package com.sda.javagda22.projekt_roman;

public class MainSymulatorFarby {

    public static int obliczZapotrzebowanieNaFarbe(Figura[] figury, double pojemnosc) {
        double sumaPowierzchni = 0.0;
        for (Figura figura : figury) {
            sumaPowierzchni += figura.obliczPole();
        }

        // Metody Math.ceil, Math.floor
        // ceil - sufit (zaokrąglenie w górę)
        // floor - podłoga (zaokrąglenie w dół)
        return (int) Math.ceil(sumaPowierzchni / pojemnosc);
    }
}
