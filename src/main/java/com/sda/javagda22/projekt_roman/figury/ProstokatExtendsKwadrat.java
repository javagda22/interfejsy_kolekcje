package com.sda.javagda22.projekt_roman.figury;

public class ProstokatExtendsKwadrat extends Kwadrat {
    private double b;

    public ProstokatExtendsKwadrat(double a, double b) {
        super(a);
        this.b = b;
    }

    @Override
    public double obliczPole() {
        return a * b;
    }

    @Override
    public double obliczObwod() {
        return 2 * (a + b);
    }
}
