package com.sda.javagda22.projekt_roman.figury;

import com.sda.javagda22.projekt_roman.Figura;

public class Trapez extends Figura {
    private double a, b, c, d, h;

    public Trapez(double a, double b, double c, double d, double h) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
        this.h = h;
    }

    public double obliczPole() {
        return ((a + b) * h) / 2.0;
    }

    public double obliczObwod() {
        return a + b + c + d;
    }
}
