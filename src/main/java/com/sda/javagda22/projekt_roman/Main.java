package com.sda.javagda22.projekt_roman;

import com.sda.javagda22.projekt_roman.figury.Koło;
import com.sda.javagda22.projekt_roman.figury.Kwadrat;
import com.sda.javagda22.projekt_roman.figury.Prostokąt;
import com.sda.javagda22.projekt_roman.figury.Trapez;

public class Main {
    public static void main(String[] args) {
        Figura[] tablicaFigur = new Figura[4];

        tablicaFigur[0] = new Kwadrat(5);
        tablicaFigur[1] = new Prostokąt(5, 10);
        tablicaFigur[2] = new Trapez(5, 10, 4, 30, 20);
        tablicaFigur[3] = new Koło(55);

        double sumaPól = 0.0;
        // pętla for i
        // używamy kiedy jest nam potrzebne wykorzystanie indeksu
        // (inne niż iterowanie) <- czyli inne niż adresowanie obiektu w [i]
        for (int i = 0; i < tablicaFigur.length; i++) {
            Figura figura = tablicaFigur[i];
            sumaPól += figura.obliczPole();
        }

        sumaPól = 0.0;
        for (Figura figura : tablicaFigur) {
//                  ^ zmienna      ^ kolekcja iterowana
            sumaPól += figura.obliczPole();
        }

        System.out.println("Suma pól do pomalowania: " + sumaPól);

        int iloscPojemnikow = MainSymulatorFarby.obliczZapotrzebowanieNaFarbe(tablicaFigur, 1000.0);
        System.out.println("Potrzebujemy " + iloscPojemnikow + " pojemników.");
    }
}
