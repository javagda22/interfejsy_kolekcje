package com.sda.javagda22.klasa_abstrakcyjna;

public abstract class Pojazd {
    private int iloscKol;
    private int predkosc;

    public Pojazd(int iloscKol) {
        this.iloscKol = iloscKol;
    }

    // metoda może mieć tą samą sygnaturę(nazwa+typ zwracany) ale
    // inne typy/ilość parametrów
    public void przyspiesz(int oIle) {
        predkosc += oIle;
    }

    public void przyspiesz() {
        predkosc++;
    }

    public abstract void zmienBiegNaWyzszy();
}
