package com.sda.javagda22.projekt_roman.figury;

import com.sda.javagda22.projekt_roman.Figura;

public class Kwadrat extends Figura {
    protected double a;

    // Prostokąt, Trapez, Koło //
    //

    public Kwadrat(double a) {
        this.a = a;
    }

    public double obliczPole() {
        return a * a;
    }

    public double obliczObwod() {
        return 4 * a;
    }
}
