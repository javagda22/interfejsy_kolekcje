package com.sda.javagda22.klasa_abstrakcyjna;

public class Main {
    public static void main(String[] args) {
        Pojazd pojazd = new SamochodOsobowy(4);
//        Pojazd pojazd = new Pojazd(4);

        pojazd.przyspiesz(320);
        pojazd.przyspiesz();

        // spiewaj z klasy ptak
        // spiewaj z klasy kukulka

        // polimorfizm obiektów

        // obiekt może przyjąć typ dowolnej klasy po której
        // dziedziczy, ale metoda którą wywołamy zawsze wywoła
        // się z klasy, którego typu posiadamy referencję
        //
        // metoda śpiewaj wywoła się z klasy 'Kukulka' a nie
        // klasy Ptak - mimo że 'obiekt' jest zadeklarowany
        // jako obiekt typu 'Ptak'
        //
        // Ptak obiekt = new Kukulka();
        // obiekt.spiewaj();
    }
}
