package com.sda.javagda22.projekt_roman.figury;

import com.sda.javagda22.projekt_roman.Figura;

public class Prostokąt extends Figura {
    private double a;
    private double b;

    public Prostokąt(double a, double b) {
        this.a = a;
        this.b = b;
    }

    public double obliczPole() {
        return a * b;
    }

    public double obliczObwod() {
        return 2 * (a + b);
    }
}
