package com.sda.javagda22.generyczne_pudelko;

import com.sda.javagda22.lombok_przyklad.Student;

public class Main {
    public static void main(String[] args) {
        // deklaruję pudełko w którym przechowam studenta
        Pudelko<Student> pudelko = new Pudelko<>();

        // ustawiam zawartość - zwróć uwagę na podpowiedź
        // sugerującą parametr setter'a jako obiekt
        // typu "Student"
        pudelko.setZawartość(new Student());

        // wyciągam studenta z pudełka
        Student studentZPudelka = pudelko.getZawartość();

    }
}
