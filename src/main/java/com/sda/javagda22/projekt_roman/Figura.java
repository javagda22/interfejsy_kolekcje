package com.sda.javagda22.projekt_roman;

public abstract class Figura {

    public abstract double obliczPole();
    public abstract double obliczObwod();
}
