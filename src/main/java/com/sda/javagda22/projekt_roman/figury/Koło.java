package com.sda.javagda22.projekt_roman.figury;

import com.sda.javagda22.projekt_roman.Figura;

public class Koło extends Figura {
    private double r;

    public Koło(double r) {
        this.r = r;
    }

    public double obliczPole() {
        return Math.PI * r * r;
    }

    public double obliczObwod() {
        return 2 * Math.PI * r;
    }
}
