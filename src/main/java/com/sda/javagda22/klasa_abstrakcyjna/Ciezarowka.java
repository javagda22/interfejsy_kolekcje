package com.sda.javagda22.klasa_abstrakcyjna;

public class Ciezarowka extends Pojazd {
    private double bieg;

    public Ciezarowka(int iloscKol) {
        super(iloscKol);
        bieg = 0.0;
    }

    public void zmienBiegNaWyzszy() {
        bieg += 0.5;
    }
}
