package com.sda.javagda22.klasa_abstrakcyjna;

public class SamochodOsobowy extends Pojazd {
    private int bieg;

    public SamochodOsobowy(int iloscKol) {
        super(iloscKol);
        bieg = 0;
    }

    public void zmienBiegNaWyzszy() {
        bieg++;
    }
}
