package com.sda.javagda22.kolekcje;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
//        Ptak ptak = Kukułka();

        List<String> list = new ArrayList<>();
        // ^ interfejs          ^ klasa końcowa

        list.add("Paweł");
        list.add("Gaweł");
        list.add("Marian");

        System.out.println(list);

        list.remove(1);

        System.out.println(list);
        System.out.println(list.size());

        list.add(null);

        System.out.println(list);
        System.out.println(list.size());

    }
}
