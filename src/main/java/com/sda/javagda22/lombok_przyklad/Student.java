package com.sda.javagda22.lombok_przyklad;

import lombok.*;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class Student {
    private String imie, nazwisko, index;
    private double oceny[];
    private ListaUwag uwagi = new ListaUwag();
}

// dziedziczenie -
// kompozycja - składowe/pola które tworzą obiekt (ListaUwag
//                  jest polem klasy Student)